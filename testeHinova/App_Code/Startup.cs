﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(testeHinova.Startup))]
namespace testeHinova
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
