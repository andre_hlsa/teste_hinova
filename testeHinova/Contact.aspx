﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Contact.aspx.cs" Inherits="Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        #Associado {
            width: 30%;
            height: 60%;
        }

        #Amigo {
            width: 30%;
            height: 35%;
        }
        .buttonBox {
    position: absolute;
    bottom:30%;
    right:50%;
}
    </style>

    <h3>Indicações</h3>
    <div class="Container">
        <div id="Associado" class="well col-xs-6" align="center">
            <h3>Associado</h3>
            <div class="form-group">
                <label for="codAssociacao">Código associação:</label>
                <input type="text" class="form-control" id="codAssociacao" name="codAssociacao">
            </div>
            <div class="form-group">
                <label for="DataCriacao">Data criação:</label>
                <input type="text" class="form-control" id="DataCriacao" name="DataCriacao">
            </div>

            <div class="form-group">
                <label for="CPF">CPF:</label>
                <input type="text" class="form-control" id="CPF" name="CPF">
            </div>
            <div class="form-group">
                <label for="NomeAssociado">Nome:</label>
                <input type="text" class="form-control" id="NomeAssociado" name="NomeAssociado">
            </div>
            <div class="form-group">
                <label for="NomeAssociado">Email:</label>
                <input type="text" class="form-control" id="EmailAssociado" name="EmailAssociado">
            </div>

            <div class="form-group">
                <label for="TelefoneAssociado">Telefone:</label>
                <input type="text" class="form-control" id="TelefoneAssociado" name="TelefoneAssociado">
            </div>
            <div class="form-group">
                <label for="PlacaAssociado">Placa Veículo:</label>
                <input type="text" class="form-control" id="PlacaAssociado" name="PlacaAssociado">
            </div>
        </div>
        <div id="Amigo" class="well col-xs-6" align="center">
            <h3>Amigo</h3>
            <div class="form-group">
                <label for="NomeAmigo">Nome:</label>
                <input type="text" class="form-control" id="NomeAmigo" name="NomeAmigo">
            </div>
            <div class="form-group">
                <label for="TelefoneAmigo">Telefone:</label>
                <input type="text" class="form-control" id="TelefoneAmigo" name="TelefoneAmigo">
            </div>
            <div class="form-group">
                <label for="EmailAmigo">Email:</label>
                <input type="text" class="form-control" id="EmailAmigo" name="EmailAmigo">
            </div>
            <div class="form-group">
                <label for="EmailAmigo">Observações:</label>
                <input type="text" class="form-control" id="Observacao" name="Observacao">
            </div>
        </div>
            <div class="buttonBox">
        <asp:Button Text="Enviar" runat="server" class="btn" OnClick="Unnamed1_Click1" />
    </div>
    </div>

</asp:Content>
