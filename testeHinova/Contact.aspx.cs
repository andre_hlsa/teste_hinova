﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Contact : Page
{
    public class ClasseEntradaIndicacao
    {
        public ClasseIndicacao Indicacao { get; set; }
        public string Remetente { get; set; }
        public string[] Copias { get; set; }
    }

    public class ClasseIndicacao
    {
        public int CodigoAssociacao { get; set; }
        public DateTime? DataCriacao { get; set; }
        public string CpfAssociado { get; set; }
        public string EmailAssociado { get; set; }
        public string NomeAssociado { get; set; }
        public string TelefoneAssociado { get; set; }
        public string PlacaVeiculoAssociado { get; set; }
        public string NomeAmigo { get; set; }
        public string TelefoneAmigo { get; set; }
        public string EmailAmigo { get; set; }
        public string Observacao { get; set; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Unnamed1_Click(object sender, EventArgs e)
    {

    }

    protected void Unnamed1_Click1(object sender, EventArgs e)
    {
        ClasseEntradaIndicacao classeent = new ClasseEntradaIndicacao();
        ClasseIndicacao classeind = new ClasseIndicacao();
        String mensagemRetorno;

        classeind.CodigoAssociacao = Convert.ToInt32(Request["codAssociacao"]);
        classeind.DataCriacao = Convert.ToDateTime(Request["DataCriacao"]);
        classeind.CpfAssociado = Request["CPF"];
        classeind.EmailAssociado = Request["EmailAssociado"];
        classeind.NomeAssociado = Request["NomeAssociado"];
        classeind.TelefoneAssociado = Request["TelefoneAssociado"];
        classeind.PlacaVeiculoAssociado = Request["PlacaAssociado"];
        classeind.NomeAmigo = Request["NomeAmigo"];
        classeind.TelefoneAmigo = Request["TelefoneAmigo"];
        classeind.EmailAmigo = Request["EmailAmigo"];
        classeind.Observacao = Request["Observacao"];

        classeent.Indicacao = classeind;
        classeent.Remetente = "andre.hlsa@gmail.com";

        HttpClient client;

        client = new HttpClient();
        client.BaseAddress = new Uri("http://app.hinovamobile.com.br/ProvaConhecimentoWebApi/");
        client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

        var result = System.Text.Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(classeent));
        var byteContent = new ByteArrayContent(result);

        System.Net.Http.HttpResponseMessage response = client.PostAsync("Api/Indicacao", byteContent).Result;

        if (response.IsSuccessStatusCode)
        {
            mensagemRetorno = "Indicacao enviada com sucesso!";
        }
        else
            mensagemRetorno = response.ReasonPhrase;

        Response.Write("<script language=javascript>alert('"+mensagemRetorno+"');</script>");
    }
}