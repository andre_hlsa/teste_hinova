﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <h3>Lista de Oficinas</h3>
    <script type="text/javascript">

        $(function () {
            $("[id*=GridView1] td").click(function () {
                DisplayDetails($(this).closest("tr"));
            });
        });
        function DisplayDetails(row) {
            var message = "";
            var abreTextoPadrao = "<span style='font-weight:normal'>";
            var fechaTextoPadrao = "</span>";

            message += "Id: " + abreTextoPadrao + $("td", row).eq(0).html() + fechaTextoPadrao;
            message += "<br><br>Nome: " + abreTextoPadrao + $("td", row).eq(1).html() + fechaTextoPadrao + "<br><br>";

            $($("td", row).eq(2).html()).each(function (i, val)
            {
                if ($($(this)).attr('nome') != null && $(this).attr('value') != null) {
                    message += $($(this)).attr('nome') + " : " + abreTextoPadrao + $(this).attr('value') + fechaTextoPadrao + "<br><br>";
                }
            })

            $(".modal-body #bookId").html(message);
            $("#myModal").modal("show");
        }
    </script>
    <style type="text/css">
        .hideGridColumn {
            display: none;
        }
    </style>

    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-condensed">
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="Id" />
            <asp:BoundField DataField="Nome" HeaderText="Nome" />
            <asp:TemplateField ItemStyle-CssClass="hideGridColumn" HeaderStyle-CssClass="hideGridColumn">
                <ItemTemplate>
                    <asp:Label Enabled="false" nome="Descricao" runat="server" Value='<%# Eval("Descricao") %>' />
                    <asp:Label Enabled="false" nome="DescricaoCurta" runat="server" Value='<%# Eval("DescricaoCurta") %>' />
                    <asp:Label Enabled="false" nome="Endereco" runat="server" Value='<%# Eval("Endereco") %>' />
                    <asp:Label Enabled="false" nome="Latitude" runat="server" Value='<%# Eval("Latitude") %>' />
                    <asp:Label Enabled="false" nome="Longitude" runat="server" Value='<%# Eval("Longitude") %>' />
                    <asp:Label Enabled="false" nome="AvaliacaoUsuario" runat="server" Value='<%# Eval("AvaliacaoUsuario") %>' />
                    <asp:Label Enabled="false" nome="CodigoAssociacao" runat="server" Value='<%# Eval("CodigoAssociacao") %>' />
                    <asp:Label Enabled="false" nome="Email" runat="server" Value='<%# Eval("Email") %>' />
                    <asp:Label Enabled="false" nome="Telefone1" runat="server" Value='<%# Eval("Telefone1") %>' />
                    <asp:Label Enabled="false" nome="Telefone2" runat="server" Value='<%# Eval("Telefone2") %>' />
                    <asp:Label Enabled="false" nome="Ativo" runat="server" Value='<%# Eval("Ativo") %>' />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>

    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%) !important;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Detalhes Oficinas</h4>
                </div>
                <div class="modal-body">
                    <label id="bookId"></label>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Voltar</button>
                </div>
            </div>
        </div>
    </div>
    </div>
</asp:Content>
