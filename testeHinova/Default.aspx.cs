﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : Page
{

    public class Oficina
    {
        List<ListaOficinas> ListaOficinas;
        RetornoErro RetornoErro;
        string Token;
    }

    public class RetornoErro
    {
        public string retornoErro { get; set; }
    }
    public class ListaOficinas
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string DescricaoCurta { get; set; }
        public string Endereco { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Foto { get; set; }
        public int AvaliacaoUsuario { get; set; }
        public int CodigoAssociacao { get; set; }
        public string Email { get; set; }
        public string Telefone1 { get; set; }
        public string Telefone2 { get; set; }
        public bool Ativo { get; set; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        HttpClient client;

        client = new HttpClient();
        client.BaseAddress = new Uri("http://app.hinovamobile.com.br/ProvaConhecimentoWebApi/");
        client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

        System.Net.Http.HttpResponseMessage response = client.GetAsync("Api/Oficina?codigoAssociacao=601").Result;

        if (response.IsSuccessStatusCode)
        {
            string resultado = response.Content.ReadAsStringAsync().Result;

            dynamic resultadoDeserializado = JsonConvert.DeserializeObject(resultado);
            string token = resultadoDeserializado.Token;
            var list = resultadoDeserializado;

            GridView1.DataSource = list["ListaOficinas"];
            GridView1.DataBind();
        }
    }

    protected void GridviewLecturer_PreRender(object sender, EventArgs e)
    {
        GridView1.Columns[2].Visible = false;
    }
}